import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(phones: any, brand: any): any {
    return phones.filter(function(phone){
      if (!phones || !brand) {
           return phones;
       }
      return phone.brand.toLowerCase().inculudes(brand.toLowerCase())
    })
  }

}
