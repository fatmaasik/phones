import { BrowserModule , Title} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ProductComponent } from './product/product.component';
import  { SmartphonesComponent } from './smartphones/smartphones.component';
import { FilterPipe } from './filter.pipe';
import { AboutComponent } from './about/about.component';
import { WhatsnewComponent } from './whatsnew/whatsnew.component';
import { RewiewComponent } from './rewiew/rewiew.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'smartphones', component: SmartphonesComponent },
  { path: 'product/:id', component: ProductComponent },
  { path: 'whatsnew', component: WhatsnewComponent },
  { path: 'rewiew', component: RewiewComponent },
  { path: 'about', component: AboutComponent },
  { path: 'home', redirectTo: '/', pathMatch: 'full' }
];


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    ProductComponent,
    SmartphonesComponent,
    FilterPipe,
    AboutComponent,
    WhatsnewComponent,
    RewiewComponent,
  ],
  providers:[Title],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
