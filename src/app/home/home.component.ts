import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../data.service';
import { FilterPipe } from '../filter.pipe'
declare var firebase: any;
import { Title }     from '@angular/platform-browser';
@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [DataService, Title],
})
export class HomeComponent implements OnInit {
  products = [];
  sub;
  query = '';
  constructor(private dataService: DataService, private titleService: Title, private activatedRoute: ActivatedRoute) { }


  ngOnInit() {
  /*  this.dataService.fetchData().subscribe(
      (data) => this.products = data
    ); */
    this.sub = this.activatedRoute.queryParams
                      .subscribe(params => {
                       this.query = params['brand'] || '';
                        });
    this.fbGetData();
    console.log(this.products.length);


  }

  fbGetData(){
    // .orderByKey('brand').equalTo(this.query)
    firebase.database().ref().on('child_added', snapshot => {
      this.products.push(snapshot.val());

    })
  }


  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
  }
}
