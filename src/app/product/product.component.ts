import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';
declare var firebase: any;
@Component({
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  providers: [DataService]
})
export class ProductComponent implements OnInit {

  product = [];
  productID: any;

  constructor(private dataService: DataService, private route:ActivatedRoute) {
    this.productID = route.snapshot.params['id'] - 1;

  }


  ngOnInit() {
    firebase.database().ref('/'+this.productID).on('child_added', ( snapshot) => {
      this.product[snapshot.key] = snapshot.val();

    })


  }



}
