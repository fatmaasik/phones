import { Component, OnInit } from '@angular/core';
import { FilterPipe } from '../filter.pipe'
declare var firebase: any;
import { Title }     from '@angular/platform-browser';
@Component({
  templateUrl: './smartphones.component.html',
  styleUrls: ['../home/home.component.css', './smartphones.component.css'],
})
export class SmartphonesComponent implements OnInit {
  smartphones = [];
  constructor(private titleService: Title ) { }


  ngOnInit() {
  /*  this.dataService.fetchData().subscribe(
      (data) => this.products = data
    ); */
    this.fbGetData();
  }

  fbGetData(){
    firebase.database().ref().on('child_added', ( snapshot) => {
      this.smartphones.push(snapshot.val())
    });

  }

  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
  }
}
